'use strict';

var gulp 			= require('gulp'),
	sass 			= require('gulp-sass'),
	autoprefixer 	= require('gulp-autoprefixer'),
	fileinclude 	= require('gulp-file-include'),
	concat 			= require('gulp-concat'),
	uglify 			= require('gulp-uglify'),
	jshint 			= require('gulp-jshint'),
	cssnano 		= require('gulp-cssnano'),
	rename 			= require('gulp-rename'),
	browserSync 	= require('browser-sync').create();

var paths = {
	dist	: './',
	source	: './source',
	html	: './source/html',
	js 		: './source/js',
	scss	: './source/scss',
	assets	: './assets',
	bundles	: './assets/bundles',
	min		: './assets/min'
}

gulp.task('sass', function () {
	return gulp.src([
			paths.scss + '/*.scss'
		])
		.pipe(sass())
		.pipe(autoprefixer({
			browsers: ['last 10 versions']
		}))
		.pipe(concat('custom.css'))
		.pipe(gulp.dest(paths.bundles + '/css'))
		.pipe(rename('custom.min.css'))
		.pipe(cssnano({zindex: false}))
		.pipe(gulp.dest(paths.min + '/css'))
		.pipe(browserSync.stream());
});

gulp.task('js', function () {
	return gulp.src([
			paths.js + '/*.js'
		])
		.pipe(gulp.dest(paths.bundles + '/js'))
		.pipe(rename({
		    suffix: ".min",
		    extname: ".js"
		}))
		.pipe(uglify())
		.pipe(gulp.dest(paths.min + '/js'));
});

gulp.task('jshint', function () {
	return gulp.src(paths.js + '/*.js')
		.pipe(jshint({
			esversion: 6
		}))
		.pipe(jshint.reporter('default'))
});

gulp.task('jsreload', ['jshint', 'js'], function (done) {
	browserSync.reload();
	done();
});

gulp.task('htmlinclude', function() {
	gulp.src([paths.html + '/pages/*.html'])
		.pipe(fileinclude({
			prefix: '@@',
			basepath: '@file'
		}))
		.pipe(gulp.dest(paths.dist));
});

gulp.task('default', ['htmlinclude', 'jshint', 'js', 'sass'], function() {

	browserSync.init({
		server: paths.dist
	});

	gulp.watch([
		paths.scss + '/*.scss',
		paths.scss + '/*/*.scss',
		paths.scss + '/*/*/*.scss'
	], {cwd: './'}, ['sass']);

	gulp.watch([paths.js + '/*.js'], {cwd: './'}, ['jsreload']);

	gulp.watch([
		paths.html + '/pages/*.html',
		paths.html + '/partials/*.html'
	], {cwd: './'}, ['htmlinclude']);

	gulp.watch(paths.dist + '/*.html').on('change', browserSync.reload);

});